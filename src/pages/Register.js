import {useState, useEffect, useContext} from 'react';
import {Button, Form, Container} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'

import UserContext from '../UserContext'

import Swal from 'sweetalert2'

export default function Register(){

	const navigate = useNavigate()
	const {user} = useContext(UserContext)

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [userName, setUserName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive]= useState(false);

	useEffect(()=>{
		if(( firstName!=="" && lastName !=="" && email !== "" && mobileNo !=="" && password1 !=="" && password2 !=="") && mobileNo.length === 11 && password1 === password2){
			setIsActive(true);
		}else{
			setIsActive(false)
		}
	},[firstName, lastName, userName, email, mobileNo, password1,password2])

	const findDuplicate= (data)=>{
		data.forEach((user) => {
			if(user.email === email){
				Swal.fire({
	                    title: "Duplicate email found",
	                    icon: "error",
	                    text: "Please provide a different email."
                		})
			}else if(user.userName === userName){
				Swal.fire({
	                    title: "Duplicate username found",
	                    icon: "error",
	                    text: "Please provide a different username."
                		})
			}else if(user.mobileNo === mobileNo){
				Swal.fire({
	                    title: "Duplicate mobile number found",
	                    icon: "error",
	                    text: "Please provide a different mobile number."
                		})
			}else{
				return false
			}
		})
	}

	const registerUser = (e)=>{
		e.preventDefault()
		
		fetch(`${process.env.REACT_APP_API_URL}/users/check-user`,{
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				userName: userName,
				mobileNo: mobileNo
			})
		}).then(res => res.json())
		.then(data =>{
			console.log(data)
			if(data !==false){
				findDuplicate(data)				
			}else{
				registerIfNoUserExists()
			}
		})
		
        setEmail("");
       	setUserName("")
        setPassword1("");
        setPassword2("");
	}

	const registerIfNoUserExists =()=>{	
		fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				userName: userName,
				password: password2
			})
		}).then(res=>res.json())
		.then(data=>{
			console.log(data)
			if(data===true){
				Swal.fire({
                            title: "Registration successful",
                            icon: "success",
                            text: "Your account has been created! You can now login your account."
                        })
                      navigate('/login') 
			}else{
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
			}
		})
	}
	return(
		(user.id !== null)? 
		<Navigate to='/shop'/>
		:
		<Form onSubmit={e=>registerUser(e)}>
			<Container>
		      <Form.Group className="mb-3" controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		        type="text" 
		        placeholder="First Name" 
		        value ={firstName}
		       	onChange = {e=>setFirstName(e.target.value)}
		       	required
		        />
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
			        type="text" 
			        placeholder="Last Name" 
			        value ={lastName}
			       	onChange = {e=>setLastName(e.target.value)}
			       	required
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="email">
		        <Form.Label>Email</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="juan@mail.com" 
		        	value ={email}
			       	onChange = {e=>setEmail(e.target.value)}
			       	required
		        	/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="mobileNo">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="eg. 09123456789" 
		        	value ={mobileNo}
			       	onChange = {e=>setMobileNo(e.target.value)}
			       	required
		        	/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="userName">
		        <Form.Label>Username</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Username" 
		        	value ={userName}
			       	onChange = {e=>setUserName(e.target.value)}
			       	required
		        	/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
			        type="password" 
			        placeholder="Password" 
			        value ={password1}
			       	onChange = {e=>setPassword1(e.target.value)}
			       	required
			        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify password</Form.Label>
		        <Form.Control 
			        type="password" 
			        placeholder="Verify password" 
			        value ={password2}
			       	onChange = {e=>setPassword2(e.target.value)}
			       	required
			        />
		      </Form.Group>
		   
		   	{(isActive)?
		   		<Button variant="primary" type="submit">
		        Submit
		      	</Button>
		      :
		      	<Button variant="primary" type="submit" disabled>
		        Submit
		      	</Button>
		       }
		      
		     </Container>
		</Form>
	)
}
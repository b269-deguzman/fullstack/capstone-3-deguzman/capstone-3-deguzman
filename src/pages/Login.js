 import {Form, Button, Container} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'

 export default function Login(){
 	const [userName, setUserName] = useState("");
 	const [password, setPassword] = useState("");
 	const {user,setUser} = useContext(UserContext);
 	const [isActive, setIsActive] = useState(false)

useEffect(()=>{
	if(userName !== '' && password !== ''){
		setIsActive(true)
	}else{
		setIsActive(false)
	}
},[userName,password])

const loginUser = (e)=>{
 		e.preventDefault()

 		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
 			method: 'POST',
 			headers: {'Content-Type': 'application/json'},
 			body: JSON.stringify({
 				userName: userName,
 				password: password
 			})
 		}).then(res=> res.json())
 		.then(data =>{
 			console.log(data)
 			if(typeof data.access !== 'undefined'){
 				
 				localStorage.setItem('token', data.access)
 				getUserDetails(data.access)
 				Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to ecommerce app"
                })

 			}else{
 				Swal.fire({
                            title: "Login Unsuccessful",
                            icon: "error",
                            text: "Please try again."
                        })
 			}
 		})
 		setUserName("");
 		setPassword("");
 	}

const getUserDetails= (token)=>{
	fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
		headers:{
			Authorization: `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data =>{
		console.log(data)
		setUser({
			id: data._id,
			userName: data.userName,
			isAdmin: data.isAdmin
		})
		console.log(user)
	})
}

 	return(

 		(user.id !== null && user.isAdmin == false)
 		?
 		<Navigate to="/shop"/>
 		:
 		(user.id !== null && user.isAdmin == true)
 		?
 		<Navigate to="/admin-dashboard"/>
 		:
	 		<Form onSubmit={e => loginUser(e)}>
				<Container>
			      <Form.Group className="mb-3" controlId="userName">
			        <Form.Label>Username</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Username" 
			        	value ={userName}
				       	onChange = {e=>setUserName(e.target.value)}
			        	/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
				        type="password" 
				        placeholder="Password" 
				        value ={password}
				       	onChange = {e=>setPassword(e.target.value)}
				        />
			      </Form.Group>

			   	{ isActive ?  
			   		<Button variant="primary" type="submit">
			        Login
			      	</Button>
			      :
			      	<Button variant="primary" type="submit" disabled>
			        Login
			      	</Button>
			    }
	   
			     </Container>
			</Form>

 	)
 }
import ProductsCard from '../components/ProductsCard'
import {useState, useEffect} from 'react'

export default function Products() {

	const [products, setProducts] = useState([])

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(products =>{
			setProducts(products.map(item =>{
				return (<ProductsCard key={item._id} item={item} />)
			}))
		})
	},[])

	return(
		<>
			{products}
		</>
	)
}
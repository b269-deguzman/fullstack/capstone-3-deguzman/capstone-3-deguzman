import {useState, useEffect} from 'react'



export default function Cart() {

	const [cartItem, setCartItem] = useState([])

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/`)
	})

	return(
	<form>
		<div className="cart-container">
			<div className="cart-block">
				<div className="cart-img"></div>
				<div className="cart-block-content">
					<div className="cart-details"></div>
					<div className="cart-quantity"></div>
					<div className="cart-pricing"></div>
				</div>
				
			</div>
			
		</div>
	</form>
	)
}
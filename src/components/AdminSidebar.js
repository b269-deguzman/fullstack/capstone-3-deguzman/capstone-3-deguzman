
import {CDBSidebar,CDBSidebarContent,CDBSidebarFooter,CDBSidebarHeader,CDBSidebarMenu,CDBSidebarMenuItem} from 'cdbreact';
import {NavLink} from 'react-router-dom'
import {useState} from 'react'


export default function AdminSidebar() {


  return (
    <div style={{ display: 'flex', height: '100vh', overflow: 'scroll initial', float: 'left' }}>

          <CDBSidebar textColor="#F1F6F9" backgroundColor="#212A3E">
            <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
              <a href="/" className="text-decoration-none" style={{ color: 'inherit' }}>
                Admin
              </a>
            </CDBSidebarHeader>

            <CDBSidebarContent className="sidebar-content">
              <CDBSidebarMenu >
                <NavLink to="/admin-dashboard/products" activeClassName="activeClicked">
                  <CDBSidebarMenuItem icon="columns" >Products</CDBSidebarMenuItem>
                </NavLink>
                <NavLink  to="/admin-dashboard/orders" activeClassName="activeClicked">
                  <CDBSidebarMenuItem icon="table" >Orders</CDBSidebarMenuItem>
                </NavLink>
                <NavLink to="/admin-dashboard/users" activeClassName="activeClicked">
                  <CDBSidebarMenuItem icon="user" >Users</CDBSidebarMenuItem>
                </NavLink>
               {/* <NavLink to="/analytics" activeClassName="activeClicked">
                  <CDBSidebarMenuItem icon="chart-line">Analytics</CDBSidebarMenuItem>
                </NavLink>

                <NavLink to="/hero404" target="_blank" activeClassName="activeClicked">
                  <CDBSidebarMenuItem icon="exclamation-circle">404 page</CDBSidebarMenuItem>
                </NavLink>*/}
              </CDBSidebarMenu>
            </CDBSidebarContent>

          </CDBSidebar>
        </div>
  );
}


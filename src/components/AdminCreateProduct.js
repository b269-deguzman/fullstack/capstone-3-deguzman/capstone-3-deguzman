import {Button, Form, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react'
import Swal from 'sweetalert2'
import {Link, useNavigate} from 'react-router-dom'

export default function AdminCreateProduct(){

	const [productName, setProductName] = useState("")
	const [productDescription, setDescription] = useState("")
	const [productPrice, setPrice] = useState(0)

	const [isActive, setIsActive] = useState(false)

	const navigate = useNavigate()

	useEffect(()=>{
		if(productName !=="" && productDescription !== "" && productPrice !== 0){
			setIsActive(true);
		}else{
			setIsActive(false)
		}
	},[productName, productDescription, productPrice])

	const createProduct =(e)=>{
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/create`,{
			method: 'POST',
			headers: {'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
		},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		}).then(res=> res.json())
		.then(data =>{
			console.log(data)
			if(data !== false){
				Swal.fire({
                            title: "Product Creation successful",
                            icon: "success",
                            text: "Product is created"
                        })
			}else{
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
			}
		})
		setProductName("")
		setDescription("")
		setPrice("")
	}

	return(
		<>
		<Form onSubmit={e=>createProduct(e)}>
			<Container>
		      <Form.Group className="mb-3" controlId="productName">
		        <Form.Label>Product Name</Form.Label>
		        <Form.Control 
		        type="text" 
		        placeholder="Product Name" 
		        value ={productName}
		       	onChange = {e=>setProductName(e.target.value)}
		       	required
		        />
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="productDescription">
		        <Form.Label>Product Description</Form.Label>
		        <Form.Control 
			        as="textarea" 
			        rows={3}
			        placeholder="Description" 
			        value ={productDescription}
			       	onChange = {e=>setDescription(e.target.value)}
			       	required
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="productPrice">
		        <Form.Label>Product Price (PHP)</Form.Label>
		        <Form.Control 
		        	type="number" 
		        	placeholder="0" 
		        	value ={productPrice}
			       	onChange = {e=>setPrice(e.target.value)}
			       	required
		        	/>
		      </Form.Group>

		   
		   	{(isActive)?
		   		<Button variant="primary" type="submit">
		        Submit
		      	</Button>
		      :
		      	<Button variant="primary" type="submit" disabled>
		        Submit
		      	</Button>
		       }
		     <Button variant="light" as={Link} to='/admin-dashboard/products'>
		        Back
		      	</Button>
		     </Container>
		</Form>
		</>
	)
}
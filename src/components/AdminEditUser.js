import {Button, Form, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react'
import Swal from 'sweetalert2'
import {Link, useNavigate, useParams} from 'react-router-dom'
export default function AdminEditUser (){
	const [isAdmin, setIsAdmin] = useState(false)
	const {userId}= useParams();
	const [isActive, setIsActive] = useState(false)
	const navigate = useNavigate()
 	
 	const handleClose = () => {
 		setIsActive(false)
 
 	}
	const handleShow = (e) => {
		
		setIsActive(e)
		setIsAdmin(true)
};

	const setUser = (e)=>{
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/set-user`,{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data =>{
			if(data){
				Swal.fire({
                            title: "Set user successful",
                            icon: "success",
                            text: `${data.message}`
                        })
					navigate('/admin-dashboard/users')
			}else{
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
			}
		})

	}
	return(

		<Container>
		<div><h1>Edit User</h1></div>
		<Form onSubmit={e=>setUser(e)}>
		      <Form.Group className="mb-3" controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		        type="text" 
		        placeholder="First Name" 
		        disabled
		        />
		      </Form.Group>
		        <Form.Group className="mb-3" controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        type="text" 
		        placeholder="Last Name" 
		        disabled
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="productPrice">
		        <Form.Label>Admin</Form.Label>
		        <Form.Check 
		        	type="switch" 
		        	value ={isAdmin}
			       	onChange = {e=> handleShow(e.target.value)}
			       	required
		        	/>
		      </Form.Group>
	
		     
		   	{(isActive)?
		   		<Button variant="primary" type="submit">
		        Save
		      	</Button>
		      :
		      	<Button variant="primary" type="submit" disabled>
		        Save
		      	</Button>
		       }
		     <Button variant="light" as={Link} to='/admin-dashboard/users'>
		        Back
		      	</Button>
		     
		</Form>
		</Container>

	)


}
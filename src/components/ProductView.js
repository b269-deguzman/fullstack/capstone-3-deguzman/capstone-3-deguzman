import {Container, Card, Button, Row , Col, Form} from 'react-bootstrap';

import {useParams, Link} from 'react-router-dom'

import {useState, useEffect, useContext} from 'react'

import UserContext from '../UserContext'

import Swal from 'sweetalert2'

export default function ProductView() {
	
	const {productId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [quantity, setQuantity] = useState(null)

	const {user} = useContext(UserContext)

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res=> res.json())
		.then(product =>{
			setName(product.name)
			setDescription(product.description)
			setPrice(product.price)
		})
	},[productId])

	const checkout =(e)=>{
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/orders/${productId}/checkout`,{
			method: 'POST',
			headers:{'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity
			})
		}).then(res	=> res.json())
		.then(data =>{
			if(data){
				Swal.fire({
	                    title: "Checkout order successful",
	                    icon: "success",
	                    text: "Please settle your payment."
                		})
			}else{
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
			}
		})
	}

	return(

		<Container>
			<Form onSubmit={e=>checkout(e)}>
			<Row className="mt-3 mb-3">
				<Col lg={{span: 6, offset:3}} bg={'primary'}>
					<Card style={{ width: '18rem' }}>
				      <Card.Img variant="top" src="" />
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Title>{description}</Card.Title>
				        <Card.Text>{price}</Card.Text>
				        <Card.Text>Quantity</Card.Text>
				        <Form.Group>
				        <Form.Control
				            required
				            type="number"
				            value={quantity}
				            onChange={e => setQuantity(e.target.value)}
				        />
				        </Form.Group>
				        {
				        	(user.id!==null)? 
				        	<>
				        	<Button className="mt-3 mb-3" variant="primary" type="submit">Checkout</Button>
				        	<Button className="mt-1 mb-1" as={Link} to='/shop' variant="primary">Continue Shopping</Button>
				        	</>
				        	:
				        	<Button className="btn btn-danger" as={Link} to="/login" >Log in to order</Button>
				        }
				      </Card.Body>
				    </Card>
				</Col>
		    </Row>
		    </Form>
   		</Container>
 
	)
}
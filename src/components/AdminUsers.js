import {Table, Container, Button} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import AdminSidebar from './AdminSidebar'

export default function	AdminUsers(){
	const [users, setUsers] = useState([])



	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/users/`,{
			headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(users =>{
			
			setUsers(users.map(item =>{
				console.log(item)
				
				return (
					<>
					<tr>
					<td>{item._id}</td>
					<td>{item.firstName}</td>
					<td>{item.lastName}</td>
					<td>{item.email}</td>
					<td>{item.userName}</td>
					<td>{item.mobileNo}</td>
					<td>{item.isAdmin.toString()}</td>
					<td><Button as={Link} to={`/admin-dashboard/users/${item._id}/edit-user`} variant="primary" type="submit">
			        Edit
			      	</Button></td>	

					</tr>

					</>
					)
			}))

		})
	},[])
	return(
		<>
		<AdminSidebar/>
		<Container>
			<div><h1>Users</h1></div>
	   <Table striped>
	     <thead>
	       <tr>
	         <th>User ID</th>
	         <th>First Name</th>
	         <th>Last Name</th>
	         <th>Email</th>
	         <th>Username</th>
	         <th>Mobile Number</th>
	         <th>isAdmin</th>
	       </tr>
	     </thead>
			<tbody>
				{users}

				{/*<>
				<tr>
					<td>{_id}</td>
			        <td>{name}</td>
			        <td>{description}</td>
			        <td>{price}</td>
			        <td>{status}</td>
			        <td>{createdOn}</td>
			        <td><Button variant="primary" type="submit">
			        Edit
			      	</Button></td>	
			      	<td><Button variant="danger" type="submit">
			        Delete
			      	</Button></td>			        
				</tr>
				</>*/}
			</tbody>	
	   </Table>
	   </Container>
		</>
	)
}
import {Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'

import UserContext from '../UserContext'
import {Link, NavLink} from 'react-router-dom'
import {useContext} from 'react'

export default function AppNavbar() {
  const {user} = useContext(UserContext)
  return (
    <Navbar className="navbar-bg" expand="lg">
      <Container>
        <Navbar.Brand className="navbar-text" as={NavLink} to='/'>Ecommerce</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link className="navbar-text" as={NavLink} to='/'>Home</Nav.Link>
             
            { (user.id !== null && user.isAdmin !== true ) ? 
              <>
              <Nav.Link className="navbar-text"  as={NavLink} to='/shop'>Shop</Nav.Link> 
              <Nav.Link className="navbar-text"  as={NavLink} to='/logout'>Logout</Nav.Link>
              </>
              :
              (user.id !== null && user.isAdmin !== false) 
              ? 
                <>
                <Nav.Link className="navbar-text"  as={NavLink} to='/admin-dashboard'>Dashboard</Nav.Link> 
                <Nav.Link className="navbar-text"  as={NavLink} to='/logout'>Logout</Nav.Link>
                </>
               :
                <>
              <Nav.Link className="navbar-text"  as={NavLink} to='/login'>Login</Nav.Link>
              <Nav.Link className="navbar-text"  as={NavLink} to='/register'>Register</Nav.Link>
              </>
            }
            {/*<NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown>*/}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
   
  );
}


import {Button, Card, Col, Container ,Row} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function ProductsCard ({item}){

	const {name, price, _id} = item

	return(
	<Container>
	<Row className="mt-3 mb-3">
		<Col lg={4} bg={'primary'}>
			<Card className="cardHighlight p-0" style={{ width: '18rem' }}>
		      <Card.Img variant="top" src="" />
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Text>{price}</Card.Text>
		        <Button variant="primary" as={Link} to={`/shop/${_id}`}>View Product</Button>
		      </Card.Body>
		    </Card>
		</Col>
    </Row>
    </Container>
	)
}
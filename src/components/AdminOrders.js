import {Table, Container, Button} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import AdminSidebar from './AdminSidebar'
export default function	AdminOrders(){

	const [orders, setOrders] = useState([])
	const [index, setIndex] = useState(0)
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/orders/`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(orders =>{
			
			setOrders(orders.map(item =>{
				console.log(item)
				setIndex(item.products.map(product=>{
					return item.products.indexOf(product)
				}))
				return (
					<>
					<tr>
					<td>{item.userId}</td>
					<td>{item.products[index].productId}</td>
					<td>{item.products[index].quantity}</td>
					<td>{item.products[index].productName}</td>
					<td>{item.totalAmount}</td>
					<td>{item.purchasedOn}</td>

					</tr>

					</>
					)
			}))

		})
	},[])
	return(
		<>
		<AdminSidebar/>
		<Container>

			<div><h1>Orders</h1></div>
			

	   <Table striped>
	     <thead>
	       <tr>
	         <th>Order ID</th>
	         <th>Product ID</th>
	         <th>Quantity</th>
	         <th>Product Name</th>
	         <th>Total Amount</th>
	         <th>Purchase Date</th>
	       </tr>
	     </thead>
			<tbody>
				{orders}
			</tbody>	
	   </Table>
	   </Container>
		</>
	)
}
import {Button, Modal, Container, Card} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Link, useParams} from 'react-router-dom'
import Swal from 'sweetalert2'
export default function AdminDisableProduct(){
	
	const {productId} = useParams();

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [productName, setProductName] = useState("")
	const [productPrice, setPrice] = useState(null)
	const [productDescription, setDescription] = useState("")
	const [productIsActive, setProductIsActive] = useState(true)


	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(product=>{
			setProductName(product.name)
			setPrice(product.price)
			setDescription(product.description)
			setProductIsActive(product.isActive)
		})
	},[productName, productDescription,productPrice, productIsActive, productId])

	const disableProduct = ()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data =>{
			console.log(data)
			if(data){
				Swal.fire({
                            title: `Disable ${data.product.name} is successful`,
                            icon: "success",
                            text: `${data.product.name} is disabled`
                        })
			}else{
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
			}
		})
	}

	const enableProduct = ()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`,{
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res => res.json())
		.then(data =>{
			if(data){
				Swal.fire({
                            title: `Activate ${data.product.name} is successful`,
                            icon: "success",
                            text: `${data.product.name} is activated`
                        })
			}else{
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
			}
		})
	}

	return(
		<>
		<Container>
		<Card>
	      <Card.Header as="h5">Product:</Card.Header>
	      <Card.Body>
	        <Card.Title>{productName}</Card.Title>
	        <Card.Text>
	          {productDescription}
	        </Card.Text>
	        <Card.Text>
	          {productPrice}
	        </Card.Text>
	          { (productIsActive===true) ?
	          	<>
	          	<Button variant="danger" onClick={handleShow}>Disable</Button>
	          	<Button as={Link} to="/admin-dashboard/products" variant="primary" >Back</Button>
	          	</>
	          	:
	          	<>
	          	<Button variant="warning" onClick={handleShow}>Enable</Button>
	          	<Button as={Link} to="/admin-dashboard/products" variant="primary" >Back</Button>
	          	</>
	          }
	      </Card.Body>
    	</Card>
    
       </Container>


      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Disable Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to disable the product?
        </Modal.Body>
        <Modal.Footer>
          {(productIsActive === true)?
          	<>
          	<Button variant="secondary" onClick={handleClose}>Close</Button>
            <Button variant="primary" type="submit" onClick={disableProduct}>Disable</Button>
            </>
            :
            <>
            <Button variant="secondary" onClick={handleClose}>Close</Button>
            <Button variant="primary" type="submit" onClick={enableProduct}>Enable</Button>
            </>
        }
        </Modal.Footer>
      </Modal>

    </>
	)
}
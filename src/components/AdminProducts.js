
import {Table, Container, Button} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import AdminSidebar from './AdminSidebar'
export default function	AdminProducts(){


	const [product, setProducts] = useState([])



	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(products =>{
			console.log(products)
			setProducts(products.map(item =>{
				console.log(item)
				
				return (
					<>
					<tr>
					<td>{item._id}</td>
					<td>{item.name}</td>
					<td>{item.description}</td>
					<td>{item.price}</td>
					<td>{item.isActive.toString()}</td>
					<td>{item.createdOn}</td>
					<td><Button as={Link} to={`/admin-dashboard/products/${item._id}/edit-product`}variant="primary" type="submit">
			        Edit
			      	</Button></td>	
			      	<td>
			      		{(item.isActive === true)?
			      		<>
			      		<Button variant="danger" type="submit" as={Link} to={`/admin-dashboard/products/${item._id}/disable-product`}>
					        Disable
					      	</Button>
					   
					     </>
					      	:
					      	<Button variant="success" type="submit" as={Link} to={`/admin-dashboard/products/${item._id}/disable-product`}>
					        Enable
					      	</Button>
					      	 }
					      	
			      	</td>
					</tr>

					</>
					)
			}))

		})
	},[])

	

	return (
	<>
	<AdminSidebar/>
	<Container>
		<div>
			<div><h1>Products</h1></div>
			<div>
			<Button as={Link} to='/admin-dashboard/products/create-product' variant="dark" type="submit">
			    Create Product
			</Button></div>
		</div>
	   <Table striped>
	     <thead>
	       <tr>
	         <th>Product ID</th>
	         <th>Name</th>
	         <th>Description</th>
	         <th>Price</th>
	         <th>Status</th>
	         <th>Date created</th>
	       </tr>
	     </thead>
			<tbody>
				{product}

				{/*<>
				<tr>
					<td>{_id}</td>
			        <td>{name}</td>
			        <td>{description}</td>
			        <td>{price}</td>
			        <td>{status}</td>
			        <td>{createdOn}</td>
			        <td><Button variant="primary" type="submit">
			        Edit
			      	</Button></td>	
			      	<td><Button variant="danger" type="submit">
			        Delete
			      	</Button></td>			        
				</tr>
				</>*/}
			</tbody>	
	   </Table>
	   </Container>
	  </>
	 );
}
import {Button, Form, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react'
import Swal from 'sweetalert2'
import {Link, useNavigate, useParams} from 'react-router-dom'

export default function AdminEditProduct(){

	const [productName, setProductName] = useState("")
	const [productDescription, setDescription] = useState("")
	const [productPrice, setPrice] = useState(null)
	const [isActive, setIsActive] = useState(false)
	const {productId}= useParams();

	const navigate = useNavigate()

	const updateProduct = (e)=>{
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`,{
			method: 'PUT',
			headers: {'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		}).then(res => res.json())
		.then(data =>{
			if(data){
				Swal.fire({
                            title: "Product Update successful",
                            icon: "success",
                            text: `${data.message}`
                        })
					navigate('/admin-dashboard/products')
			}else{
				Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
			}
		})
		setProductName("")
		setDescription("")
		setPrice("")
	}

	// const changeProductStatus =()=>{
	// 	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
	// 		headers: {
	// 			Authorization: `Bearer ${localStorage.getItem('token')}`
	// 		}
	// 	}).then(res => res.json())
	// 	.then(data =>{
	// 		if(data){
	// 			setSwitchState(data.product.isActive)
	// 			Swal.fire({
    //                         title: "Archive Product successful",
    //                         icon: "success",
    //                         text: `${data.message}`
    //                     })
	// 		}else{
	// 			Swal.fire({
    //                         title: "Something went wrong",
    //                         icon: "error",
    //                         text: "Please try again."
    //                     })
	// 		}
	// 	})
	// }

	useEffect(()=>{
		if(productName !== "" && productDescription !== "" && productPrice !== null){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[productName, productDescription, productPrice])

	return(
		<>
		<Container>
		<div><h1>Edit Product</h1></div>
		<Form onSubmit={e=>updateProduct(e)}>
		      <Form.Group className="mb-3" controlId="productName">
		        <Form.Label>Product Name</Form.Label>
		        <Form.Control 
		        type="text" 
		        placeholder="New Product Name" 
		        value ={productName}
		       	onChange = {e=>setProductName(e.target.value)}
		       	required
		        />
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="productDescription">
		        <Form.Label>Product Description</Form.Label>
		        <Form.Control 
			        as="textarea" 
			        rows={3}
			        placeholder="New Product Description" 
			        value ={productDescription}
			       	onChange = {e=>setDescription(e.target.value)}
			       	required
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="productPrice">
		        <Form.Label>Product Price (PHP)</Form.Label>
		        <Form.Control 
		        	type="number" 
		        	placeholder="1000"
		        	value ={productPrice}
			       	onChange = {e=>setPrice(e.target.value)}
			       	required
		        	/>
		      </Form.Group>
	
		     
		   	{(isActive)?
		   		<Button variant="primary" type="submit">
		        Save
		      	</Button>
		      :
		      	<Button variant="primary" type="submit" disabled>
		        Save
		      	</Button>
		       }
		     <Button variant="light" as={Link} to='/admin-dashboard/products'>
		        Back
		      	</Button>
		     
		</Form>
		</Container>
		</>
	)
}
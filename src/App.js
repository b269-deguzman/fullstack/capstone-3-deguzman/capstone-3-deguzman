import AppNavbar from './components/AppNavbar'
import AdminSidebar from './components/AdminSidebar'
import Register from './pages/Register'
import Login from './pages/Login'
import Home from './pages/Home'
import Shop from './pages/Shop'
import ProductView from './components/ProductView'
import Logout from './pages/Logout'
import Admin from './pages/Admin'
import AdminProducts from './components/AdminProducts'
import AdminOrders from './components/AdminOrders'
import AdminUsers from './components/AdminUsers'
import AdminCreateProduct from './components/AdminCreateProduct'
import AdminEditProduct from './components/AdminEditProduct'
import AdminDisableProduct from './components/AdminDisableProduct'
import AdminEditUser from './components/AdminEditUser'
import {UserProvider} from './UserContext'

import {useState, useEffect} from 'react'

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'


import './App.css';

function App() {

  const [user, setUser]= useState({id: null, userName: null, isAdmin: null})


  const unsetUser = ()=>{
    localStorage.clear()
  }

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
    .then(data =>{
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          userName: data.userName,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null, userName: null, isAdmin: null
        })
      }
    })
  },[]);


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}} >
    <Router>
      <AppNavbar/>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/shop" element={<Shop/>}/>
          <Route path="/shop/:productId" element={<ProductView/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element= {<Logout/>}/>
          <Route path="/admin-dashboard" element={<Admin/>}/>
          <Route path="/admin-dashboard/products" element={<AdminProducts/>}/>
          <Route path="/admin-dashboard/orders" element={<AdminOrders/>}/>
          <Route path="/admin-dashboard/users" element={<AdminUsers/>}/>
          <Route path="/admin-dashboard/products/create-product" element={<AdminCreateProduct/>}/>
          <Route path="/admin-dashboard/products/:productId/edit-product" element={<AdminEditProduct/>}/>
          <Route path="/admin-dashboard/users/:userId/edit-user" element={<AdminEditUser/>}/>
             <Route path="/admin-dashboard/products/:productId/disable-product" element={<AdminDisableProduct/>}/>

        </Routes>

       
    </Router>
    </UserProvider>  
    </>
  );
}

export default App;
